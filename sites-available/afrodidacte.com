server {
        listen 80;
        listen [::]:80;
        server_name afrodidacte.com;
        index index.html index.php;
        root /var/www/html/afrodidacte/public;
        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                try_files $uri $uri/ =404;
        }

        location ~ \.php$ {
      		include snippets/fastcgi-php.conf;
      	#
      	#	# With php7.1-cgi alone:
      	#	fastcgi_pass 127.0.0.1:9000;
      	#	# With php7.1-fpm:
      		fastcgi_pass unix:/run/php/php7.1-fpm.sock;
      	}

        location ~ /\.ht {
                deny all;
        }
}

server {
	listen 80;
	listen [::]:80;

	root /var/www/html/ouagasquare/public;

	index index.php index.html;

	server_name ouagasquare.com;

	location / {
		try_files $uri $uri/ =404;
	}

	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/run/php/php7.1-fpm.sock;
	}

	location ~ /\.ht {
		deny all;
	}
}
